'use strict';

// ================================================================
// get all the tools we need
// ================================================================
var express = require('express');
//var routes = require('./routes/index.js');
var port = process.env.PORT || 8080;

var app = express();

// ================================================================
// setup our express application
// ================================================================
//app.use('/public', express.static(process.cwd() + '/public'));
app.set('view engine', 'ejs');


// ================================================================
// setup routes
// ================================================================

//routes(app);


//var express = require('express');
//var app = express();
app.use(express.static(__dirname + '/public'));
//app.set('view engine', 'ejs');

app.get('/', function(req, res) {
    res.render('pages/index');

});
app.get('/signup', function(req, res) {
    res.render ('pages/signup');
}); 
app.get('/result', function(req, res) {
    res.render ('pages/result');
}); 

app.get('/data_delivery_order', function(req, res) {
    res.render ('pages/data_delivery_order');
}); 
app.get('/data_order', function(req, res) {
    res.render ('pages/data_order');
}); 
app.get('/check_status_daftar_data', function(req, res) {
    res.render ('pages/check_status_daftar_data');
}); 
app.get('/setting_account', function(req, res) {
    res.render ('pages/setting_account');
}); 
app.get('/daftar_jasa_fleet', function(req, res) {
    res.render ('pages/daftar_jasa_fleet');
});
app.get('/daftar_transaksi_fleet', function(req, res) {
    res.render ('pages/daftar_transaksi_fleet');
});
app.get('/check_status_transaksi', function(req, res) {
    res.render ('pages/check_status_transaksi');
});
app.get('/check_status_history_transaksi', function(req, res) {
    res.render ('pages/check_status_history_transaksi');
});
app.get('/detail_daftar_transaksi', function(req, res) {
    res.render ('pages/detail_daftar_transaksi');
});
app.get('/login', function(req, res) {
    res.render ('pages/login');
});
app.get('/see_all_not', function(req, res) {
    res.render ('pages/see_all_not');
});

app.get('/kode_verify', function(req, res) {
    res.render ('pages/kode_verify');
});

app.get('/forgot_password', function(req, res) {
    res.render ('pages/forgot_password');
});
app.get('/cart', function(req, res) {
    res.render ('pages/cart');
});


app.get('/list_daftar_data_fleet', function(req, res) {
    res.render ('pages/list_daftar_data_fleet');
});
app.get('/detail_daftar_jasa', function(req, res) {
    res.render ('pages/detail_daftar_jasa');
});
app.get('/check_status_data_history', function(req, res) {
    res.render ('pages/check_status_data_history');
}); 
app.get('/contact_us', function(req, res) {
    res.render ('pages/contact_us');
});
app.get('/help', function(req, res) {
    res.render ('pages/help');
}); 

 

 app.get('/404', function(req, res) {
    res.render ('pages/404');
}); 
  app.get('/500', function(req, res) {
    res.render ('pages/500');
}); 
  app.get('/401', function(req, res) {
    res.render ('pages/401');
}); 
  app.get('/503', function(req, res) {
    res.render ('pages/503');
}); 

// ================================================================
// start our server
// ================================================================
app.listen(port, function() {
    console.log('Server listening on port ' + port + '...');
});

 


//app.listen(8080);

var router = express.Router();
router.get('/', function(req, res) {
    res.send('Ini Home Page!');
});
