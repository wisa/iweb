# AIR Web Node.js Application
This is a desc web application

## Live Demonstration
[You can view this](https://localhost/)

## Requirements

- Node xx
- Express xx
- EJS xx

## Installation

```bash
git clone git@gitlab.com/wisa/iweb.git
npm install
node server.js
```

## Screenshots if ...

When you start
![Home Page](/screenshots/home.png?raw=true "Home")

The About page for exp
![About Page](/screenshots/about.png?raw=true "About")

This wisha update info

- foo
- foo2


